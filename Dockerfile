FROM jenkins/jenkins:lts-jdk11

USER root
#COPY kubectl terragrunt /var/lib/
#RUN chmod 555 /var/lib/kubectl /var/lib/terragrunt
#ENV PATH=$PATH:/var/lib
RUN apt-get update && \
apt-get -y install apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common \
    python3 \
    python3-tk \
    python3-pip \
    python3-dev \
    libxml2-dev \
    libxslt-dev \
    jq \
    zlib1g-dev \
    net-tools && \
curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; \
echo "$ID")/gpg > /tmp/dkey; apt-key add /tmp/dkey && \
add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
    $(lsb_release -cs) \
    stable" && \
apt-get update && \
apt-get -y install docker-ce && \
apt-get clean && \
rm -rf /var/lib/apt/lists/*
